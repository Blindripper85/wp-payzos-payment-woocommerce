<?php
defined('ABSPATH') || exit('No Direct Access.');

/**
 * Conseil is a blockchain indexer & API for building decentralized applications, currently focused on Tezos.
 * - [Conseil](https://github.com/Cryptonomic/Conseil/)
 * - [API document](https://github.com/Cryptonomic/Conseil/tree/master/docs)
 * - [JavaScript client](https://github.com/Cryptonomic/ConseilJS)
 * - [JavaScript client document](https://cryptonomic.github.io/ConseilJS/)
 *
 * in this class we are using Conseil openApi to check validation of a payment under Tezos Network.
 *
 * you can change WP_PAYZOS_PAYMENT_WOOCOMMERCE_env.php in root of project for change out api key and endpoint with your personal constant
 */
class WP_PAYZOS_PAYMENT_WOOCOMMERCE_Conseil
{
    private $network = "mainnet";
    private $entity = "operations";
    /**
     * constructor
     */
    public function __construct()
    {
    }

    /**
     * check if hash is avilable
     *
     * @param string $_hash_id
     * @return boolean
     */
    public function check_hash($_hash_id)
    {
        return true;
    }

    /**
     * check tezos transaction with a hash_id, amount and start time.
     *
     * @param string    $_hash_id
     * @param int       $_amount
     * @param int       $_start_time
     * @return boolean|null|array [
     *  - source,
     *  - timestamp,
     *  - operation_group_hash,
     *  - amount,
     *  - fee
     * ]
     */
    public function check_payment_is_done($_hash_id, $_amount, $_start_time)
    {
        if (!is_string($_hash_id) || !is_numeric($_amount) || !is_numeric($_start_time)) {
            return false;
        }
        $query_object = $this->make_check_transaction_request_body($_hash_id, $_amount, $_start_time);
        if (!is_array($query_object) || !$query_object) {
            return false;
        }

        $result = $this->conseil_rest_api($this->network, $this->entity, $query_object);
        if (!$result) {
            return false;
        }
        if (empty($result) || !isset($result[0])) {
            return false;
        }
        if (!isset($result[0]["timestamp"]) || !isset($result[0]["source"]) || !isset($result[0]["operation_group_hash"])  || !isset($result[0]["fee"])) {
            return false;
        }
        $result[0]["timestamp"] = $result[0]["timestamp"] / 1000;
        return $result[0];
    }


    /**
     * Undocumented function
     *
     * @param string $_network
     * @param string $_entity
     * @param array $_request_body
     * @return boolean|array
     */
    private function conseil_rest_api($_network = "", $_entity = "", $_request_body = [])
    {
        if (!is_string($_network) || !is_string($_entity) || !is_array($_request_body)) {
            return false;
        }
        $network = (!$_network == "") ? "/".$_network: "";
        $entity = (!$_entity == "") ? "/".$_entity: "";
        $url = WP_PAYZOS_PAYMENT_WOOCOMMERCE_CONSEIL_API_URL . "/v2/data/" . WP_PAYZOS_PAYMENT_WOOCOMMERCE_CONSEIL_PLATFORM . $network . $entity;
        $response = wp_remote_post(
            $url,
            [
                'body'        => wp_json_encode($_request_body),
                'headers'     => [
                    'Content-Type' => 'application/json',
                    'apiKey' => '9258f47b-4b9a-493d-977e-44a9b8ee5876'
                ],
            ]
        );
        if (is_wp_error($response)) {
            return false;
        }
        $array = json_decode($response["body"], true);
        if (!is_array($array)) {
            return false;
        }
        return $array;
    }


    /**
     * Make request body to check a transaction
     * i just make this Ts file to PHP :
     * https://github.com/Cryptonomic/ConseilJS/blob/master/src/reporting/ConseilQueryBuilder.ts
     * @param string $_destination_hash
     * @param int $_amount
     * @param int $_start_time
     * @return boolean|array
     */
    private function make_check_transaction_request_body($_destination_hash, $_amount, $_start_time)
    {
        if (!is_string($_destination_hash) || !is_numeric($_start_time) || !is_numeric($_amount)) {
            return false;
        }
        $array = [
            'fields' =>[
                "source",
                "timestamp",
                "operation_group_hash",
                "amount",
                "destination",
                "fee"
            ],
            'predicates' => [
                [
                    'field' => 'kind',
                    'operation' => 'eq',
                    'set' => ['transaction'],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => "destination",
                    'operation' => 'eq',
                    'set' => [$_destination_hash],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => 'timestamp',
                    'operation' => 'between',
                    'set' => [
                        $_start_time * 1000,
                        time() * 1000
                    ],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => 'amount',
                    'operation' => 'eq',
                    'set' => [
                        $_amount,
                    ],
                    'inverse' => false,
                    'group' => null
                ],
                [
                    'field' => 'status',
                    'operation' => 'eq',
                    'set' => ['applied'],
                    'inverse' => false,
                    'group' => null
                ],
            ],
            'orderBy' =>[
                [
                    'field' => 'timestamp',
                    'direction' => 'desc'
                ]
            ],
            'aggregation' =>[],
            'limit' => 1
    
        ];
        return $array;
    }


    /**
     * check is user input hash a valid tezos wallet hash ?
     *
     * @param string $_wallet_hash
     * @return boolean
     */
    public function validate_wallet_hash($_wallet_hash)
    {
        return true;
    }
}
