<?php
defined('ABSPATH') || exit('No Direct Access.');


class WP_PAYZOS_PAYMENT_WOOCOMMERCE_RestApi
{
    private $conseil;
    private $model;
    private $functions;
    public function __construct($_conseil, $_model, $_functions)
    {
        $this->conseil = $_conseil;
        $this->model   = $_model;
        $this->functions   = $_functions;
    }

    public function init()
    {
        register_rest_route('wp_payzos_wc/v1', 'make_payment', [
            "methods"   => "POST",
            "callback"  => [$this, "rest_make_payment"]
        ]);

        register_rest_route('wp_payzos_wc/v1', 'validate_payment', [
            "methods"   => "POST",
            "callback"  => [$this, "rest_validate_payment"]
        ]);

        register_rest_route('wp_payzos_wc/v1', 'payment_info', [
            "methods"   => "GET",
            "callback"  => [$this, "rest_payment_info"]
        ]);
    }

    /**
     * wordpress rest api callback function.
     * Method : Post
     * Access point : wp_payzos_wc/v1/make_payment
     * Input : [order_id : int, destination_hash : string]
     * Job : make and store payment detail
     *
     * @return [
     *  ok : boolean,
     *  payment_id : int
     * ]
     */
    public function rest_make_payment()
    {
        $output = [];
        $data = json_decode(file_get_contents("php://input"), true);
        if (!isset($data["order_id"]) || !isset($data["destination_hash"])) {
            $output["ok"] = false;
            $output["message"] = "fill values";
            echo json_encode($output);
            return null;
        }
        // Just check and complete if there is a not completed payment 
        $this->functions->payment_check_cron_exec($this->model);
        // sanitize order_id
        $order_id = intval($data["order_id"]);
        // get payment
        $order = wc_get_order($order_id);
        if (!$order->needs_payment()) {
            $output["ok"] = false;
            $output["message"] = "what do you want to do with complete order ?";
            echo json_encode($output);
            return null;
        }
        // sanitize destination_hash
        if (!$this->conseil->validate_wallet_hash($data["destination_hash"])) {
            $output["ok"] = false;
            $output["message"] = "no valid tezos account";
            echo json_encode($output);
            return null;
        }
        $destination_hash = $data["destination_hash"];
        
        // Get xtz amount of order total
        $order_amount = $order->get_total();
        $order_currency = $order->get_currency();
        $xtz_amount = $this->functions->get_xtz_amount($order_amount, $order_currency);
        if (!$xtz_amount || !is_numeric($xtz_amount)) {
            $output["ok"] = false;
            $output["message"] = "i can't calculate xtz value";
            echo json_encode($output);
            return null;
        }
        /**
         * we must to make a uniq payment in same time.
         * philosophy is to add a very little amount to payment amount if there is two await payment with same amount.
         *
         * and we can't to make more than 19 payment with same amount in same time.
         */
        $i = 0;
        while (!$this->model->can_pay_this_amount($xtz_amount)) {
            $i++;
            if ($i > 20) {
                $output["ok"] = false;
                $output["message"] = "more payment with same value in this time. please try another time.";
                echo json_encode($output);
                return null;
            }
            $xtz_amount++;
        }
        // unixTime is a hero
        $payment_start_time = time();
        // try to store payment
        $payment_id = $this->model->store_payment($order_id, $xtz_amount, $destination_hash, $payment_start_time);
        if (!$payment_id || !is_numeric($payment_id)) {
            $output["ok"] = false;
            $output["message"] = "sorry i can't make payment";
            echo json_encode($output);
            return null;
        }
        $output["ok"] = true;
        $output["payment_id"] = $payment_id;
        echo json_encode($output);
        return null;
    }

    /**
     * wordpress rest api callback function.
     * Method : Post
     * Access point : wp_payzos_wc/v1/validate_payment
     * Input : [payment_id : int]
     * Job : check payment status in serverside
     *
     * @return [
     *  ok : boolean
     * ]
     */
    public function rest_validate_payment()
    {
        $output = [];
        $data = json_decode(file_get_contents("php://input"), true);
        if (!isset($data["payment_id"])) {
            $output["ok"] = false;
            $output["message"] = "fill values";
            echo json_encode($output);
            return null;
        }
        // sanitize payment_id
        $payment_id = intval($data["payment_id"]);
        $landing_data = $this->process_payment_status($payment_id);
        return $landing_data;
        echo json_encode($output);
        return null;
    }

    /**
     * wordpress rest api callback function.
     * Method : Post
     * Access point : wp_payzos_wc/v1/validate_payment
     * Input : [payment_id : int]
     * Job : get payment detail
     *
     * @return [
     *  ok : boolean,
     *  date : [
     *      status: string,
     *      destination_hash : string,
     *      time : int
     *  ]
     * ]
     */
    public function rest_payment_info()
    {
        $output = [];
        if (!isset($_GET["payment_id"])) {
            $output["ok"] = false;
            $output["message"] = "fill values";
            echo json_encode($output);
            return null;
        }
        $payment_id = intval($this->functions->decrypt($_GET["payment_id"]));
        $payment = $this->model->get_payment($payment_id);
        if (!$payment || empty($payment)) {
            $output["ok"] = false;
            $output["message"] = "nothing to show";
            echo json_encode($output);
            return null;
        }
        $output["ok"] = true;
        $output["data"] = $payment;
        echo json_encode($output);
        return null;
    }


    /**
     * check and store payment status
     *
     * @param int $_payment_id
     * @return boolean
     */
    private function process_payment_status($_payment_id)
    {
        error_log("SSS");
        if (!is_numeric($_payment_id)) {
            return ["ok" =>true, "message" => "fill value"];
        }
        // get payment data from model
        $payment = $this->model->get_payment($_payment_id);
        if (!$payment) {
            return ["ok" =>true, "message" => "nothing in model"];
        }
        if (!isset($payment["destination_hash"]) || !isset($payment["start_time"]) || !isset($payment["amount"])) {
            return ["ok" =>true, "message" => "model payment wrong value"];
        }
        // Check payment
        $conseil_result = $this->conseil->check_payment_is_done($payment["destination_hash"], $payment["amount"], $payment["start_time"]);
        if (!$conseil_result || is_null($conseil_result) || !is_array($conseil_result)) {
            $store_result = $this->model->cancel_payment($_payment_id, time());
            if (!$store_result) {
                return ["ok" =>true, "message" => "can't cacel it"];
                error_log("cancel Problem");
            }
            return ["ok" =>true, "message" => "cacel done"];
        }
        $store_result = $this->model->finish_payment($_payment_id, $conseil_result["source"], $conseil_result["timestamp"], $conseil_result["operation_group_hash"]);
        if (!$store_result) {
            return ["ok" =>true, "message" => "can't store it"];
        }
        /**
         * clear cart and finish payment
         */
        $order_id = $payment["order_id"];
        $order = wc_get_order($order_id);
        $order->payment_complete();
        if(function_exists('wc_empty_cart')){
            wc_empty_cart();
        }

        return ["ok" =>true, "message" => "done"];
    }
}
