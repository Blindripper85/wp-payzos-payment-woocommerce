<?php
defined('ABSPATH') || exit('No Direct Access.');

class WP_PAYZOS_PAYMENT_WOOCOMMERCE_Functions
{
    public function __construct()
    {
    }


    /**
     * put order amount and currency type and you will get the XTZ amount.
     *
     * @param integer $_amount
     * @param string $_type
     * @return boolean|integer
     */
    public function get_xtz_amount($_amount, $_type)
    {
        if (!is_numeric($_amount) || !is_string($_type)) {
            return false;
        }
        $usd_amount = intval($_amount * 1000000);
        /**
         * we must to convert other currencies to USD
         * [fixer.io](https://fixer.io/documentation)
         * alo you can replace your fixer apikey in ROOT/WP_PAYZOS_PAYMENT_WOOCOMMERCE_ENV.php
         */
        if (strtoupper($_type) != "USD") {
            $fixer_url = "https://data.fixer.io/api/convert?access_key=".WP_PAYZOS_PAYMENT_WOOCOMMERCE_FIXER_API_KEY."&from=".$_type."&to=USD&amount=".$usd_amount;
            $fixer_response = wp_remote_get($fixer_url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                  ]
            ]);
            if (is_wp_error($fixer_response)) {
                return false;
            }
            $fixer_array = json_decode($fixer_response["body"], true);
            if (!is_array($fixer_array)) {
                return false;
            }
            if (!isset($fixer_array["result"]) || !is_numeric($fixer_array["result"])) {
                return false;
            }
            $usd_amount = $fixer_array["result"];
        }

        /**
         * we must to get price of XTZ in USD from coinmarketcap.
         * [coinMarketCap Api Document](https://coinmarketcap.com/api/)
         * we are using public api without any apiKey.
         */
        $cmk_url = "https://api.coinmarketcap.com/v1/ticker/tezos/";
        $cmk_response = wp_remote_get($cmk_url, [
            'headers' => [
                'Content-Type' => 'application/json',
              ]
        ]);
        if (is_wp_error($cmk_response)) {
            return false;
        }
        $cmk_array = json_decode($cmk_response["body"], true);
        if (!is_array($cmk_array)) {
            return false;
        }
        if (!isset($cmk_array[0]["price_usd"]) || !is_numeric($cmk_array[0]["price_usd"])) {
            return false;
        }
        $cmk_amount = intval($cmk_array[0]["price_usd"] * 1000000);
        $xtz_amount = intval(($usd_amount / $cmk_amount) * 1000000);
        return $xtz_amount;
    }


    /**
     * encrypt a String
     *
     * @param integer $_string
     * @return string
     */
    public function encrypt($_string)
    {
        if (!is_string($_string)) {
            return false;
        }
        $data = openssl_encrypt(
            $_string,
            WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_METHOD,
            hex2bin(WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_KEY),
            0,
            hex2bin(WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_IV)
        );
        $hash = base64_encode($data);
        return $hash;
    }


    /**
     * decrypt the string if it encypted with $this->encrypt method
     *
     * @param string $_hash
     * @return string
     */
    public function decrypt($_hash)
    {
        if (!is_string($_hash)) {
            return false;
        }

        $data = base64_decode($_hash);
        $un_hashed_string = openssl_decrypt(
            $data,
            WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_METHOD,
            hex2bin(WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_KEY),
            0,
            hex2bin(WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_IV)
        );
        return $un_hashed_string;
    }

    public function payment_check_cron_exec($_model)
    {
        $payments = $_model->get_long_await_payment();
        foreach ($payments as $key => $value) {
            $url = get_rest_url(null, 'wp_payzos_wc/v1/validate_payment');
            $data = [
                "payment_id" => $value["id"],
            ];
            
            $result = wp_remote_post($url, [
                'timeout'     => 4500,
                'body'    => wp_json_encode($data),
                'headers' => ['Content-Type' => 'application/json']
            ]);
            if (is_wp_error($result)) {
                error_log(json_encode($result));
            }
        }
        return true;
    }
}
