<?php
defined('ABSPATH') || exit('No Direct Access.');

include WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR . 'controller/WP_PAYZOS_PAYMENT_WOOCOMMERCE_Conseil.php';
include WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR . 'controller/WP_PAYZOS_PAYMENT_WOOCOMMERCE_RestApi.php';
include WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR . 'controller/WP_PAYZOS_PAYMENT_WOOCOMMERCE_Functions.php';
include WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR . 'model/WP_PAYZOS_PAYMENT_WOOCOMMERCE_Model.php';
include WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR . 'view/WP_PAYZOS_PAYMENT_WOOCOMMERCE_View.php';

class WP_PAYZOS_PAYMENT_WOOCOMMERCE_Controller
{
    private $view;
    private $model;
    private $conseil;
    private $functions;

    /**
     * constructor.
     * define and register some configure
     */
    public function __construct()
    {
        $this->view  = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_View();
        $this->model  = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_Model();
        $this->conseil   = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_Conseil();
        $this->functions   = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_Functions();
        $this->rest_api   = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_RestApi($this->conseil, $this->model, $this->functions);
        /**
         * initialize woocommerce payment gateway
         */
        add_filter('woocommerce_payment_gateways', [$this, "payzos_gateway_init"]);
        // add_action('plugins_loaded', [$this, 'init']);
        $this->init();
        /**
         * initialize wordpress text domain (multilanguage)
         */
        add_action('plugins_loaded', [$this, 'textdomain']);
        /**
         * initialize wordpress rest api for manage and validate our payment
         */
        add_action('rest_api_init', [$this, "payzos_rest_api"]);
        /**
         * initialize shortcode for make payment page
         */
        add_shortcode('wp-payzos-payment-woocommerce', [$this, "payzos_payment_page"]);
        /**
         * Admin Page
         */
        add_action('admin_menu', [$this, "admin_page"]);
    }


    public function payzos_gateway_init($methods)
    {
        $methods[] = 'WC_Payzos';
        return $methods;
    }

    public function init()
    {
        include WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR."controller/WC_Payzos.php";
    }

    public function textdomain()
    {
        load_plugin_textdomain('wp-payzos-payment-woocommerce', false, WP_PAYZOS_PAYMENT_WOOCOMMERCE_DIR . 'languages/');
    }

    public function payzos_rest_api()
    {
        $this->rest_api->init();
    }

    public function payzos_payment_page()
    {
        $data = [];
        if (!isset($_GET["payment_id"])) {
            return null;
        }
        $payment_id = intval($this->functions->decrypt($_GET["payment_id"]));
        $payment = $this->model->get_payment($payment_id);
        if (!$payment || empty($payment)) {
            return null;
        }
        if ($payment["status"] == 'done') {
            if (function_exists('wc_empty_cart')) {
                wc_empty_cart();
            }
        }
        $this->view->payment_page($data);
    }

    /**
     * Install method run when plugin going to enable.
     *
     * @return boolean
     */
    public static function install()
    {
        if (!WP_PAYZOS_PAYMENT_WOOCOMMERCE_Model::install()) {
            return false;
        }
        return true;
    }

    public function admin_page()
    {
        add_menu_page('Payzos transaction list', 'pyzos payment', 'manage_options', 'payzos_transaction', [$this, 'transaction_list_page']);
    }

    public function transaction_list_page()
    {
        $this->functions->payment_check_cron_exec($this->model);
        $data = [];
        $data["transactions"] = $this->model->transaction_list();
        $this->view->transaction_list_page($data);
    }
}
