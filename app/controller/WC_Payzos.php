<?php
defined('ABSPATH') || exit('No Direct Access.');

/**
 * woocommerce payment configure class
 */
class WC_Payzos extends WC_Payment_Gateway
{
    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->id = "tezos_payment_gateway";
        // $this->method_title = __("Payzos", 'wp-payzos-payment-woocommerce');
        $this->method_description = __("Payzos is Tezos payment service", 'wp-payzos-payment-woocommerce');
        $this->title = __("Payzos", 'wp-payzos-payment-woocommerce');
        $this->description = __("Payzos is Tezos payment service", 'wp-payzos-payment-woocommerce');
        $this->icon = WP_PAYZOS_PAYMENT_WOOCOMMERCE_ASSETS_URL."img/logo_40_40.png";
        $this->has_fields = true;
        $this->init_form_fields();
        $this->init_settings();

        // Turn these settings into variables we can use
        foreach ($this->settings as $setting_key => $value) {
            $this->$setting_key = $value;
        }

        // Save settings
        if (is_admin()) {
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_wallet_hash']);
        }
    }

    /**
     * will override of init_form_fields
     * wath this : https://docs.woocommerce.com/wc-apidocs/class-WC_Settings_API.html#_init_form_fields
     *
     * @return array
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title'    => __('Enable / Disable', 'wp-payzos-payment-woocommerce'),
                'label'    => __('Enable this payment gateway', 'wp-payzos-payment-woocommerce'),
                'type'    => 'checkbox',
                'default'  => 'no',
            ),
            'wallet_hash' => array(
                'title'    => __('XTZ address', 'wp-payzos-payment-woocommerce'),
                'type'    => 'text',
                'desc_tip'  => __('we need public hash of your Tezos wallet to make a payment gateway', 'wp-payzos-payment-woocommerce'),
            )
        );
    }

    /**
     *
     * i did this because we must to validate tezos wallet hash.
     *
     * @return boolean
     */
    public function process_wallet_hash()
    {
        $my_form =  $this->get_post_data();
        ;
        $wallet_hash = $my_form["woocommerce_tezos_payment_gateway_wallet_hash"];
        if (!is_string($wallet_hash)) {
            $this->add_error("wrong input");
            $this->display_errors();
            return false;
        }
        $conseil = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_Conseil();
        if (!$conseil->validate_wallet_hash($wallet_hash)) {
            $this->add_error("your wallet hash is not valid");
            $this->display_errors();
            return false;
        }
        return $this->process_admin_options();
    }

    /**
     * Undocumented function
     *
     * @param int $_order_id
     * @return void|array [result, redirect]
     */
    public function process_payment($_order_id)
    {
        $payment_id = $this->get_payment_id(
            $_order_id,
            $this->wallet_hash
        );

        if (!$payment_id) {
            wc_add_notice("problem, sssssplease contact to admin", 'error');
            return false;
        }
        $functions = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_Functions();
        $payment_id_encrypted = $functions->encrypt(strval($payment_id));
        if (!$payment_id_encrypted) {
            error_log($payment_id_encrypted);
            wc_add_notice("problem, wwwwwplease contact to admin", 'error');
            return false;
        }
        $url = get_site_url() . "/payzos/?payment_id=".$payment_id_encrypted;
        return [
            'result'   => 'success',
            'redirect' => $url,
        ];
    }

    private function get_payment_id($_order_id, $_wallet_hash)
    {
        $url = get_rest_url(null, 'wp_payzos_wc/v1/make_payment');
        $data = [
            "order_id"          => $_order_id,
            "destination_hash"  => $_wallet_hash,
        ];

        $result = wp_remote_post($url, [
            'timeout'     => 4500,
            'body'    => wp_json_encode($data),
            'headers' => ['Content-Type' => 'application/json']
        ]);
        if (is_wp_error($result)) {
            error_log(json_encode($result));
            return false;
        }

        $array = json_decode($result["body"], true);
        if (!isset($array["payment_id"])) {
            wc_add_notice($result["body"]);
            return false;
        }
        return $array["payment_id"];
    }
}
