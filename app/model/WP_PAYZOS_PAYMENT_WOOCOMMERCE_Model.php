<?php
defined('ABSPATH') || exit('No Direct Access.');

/**
 * Model Class
 */
class WP_PAYZOS_PAYMENT_WOOCOMMERCE_Model
{
    private $table_name;
    private $db;
    /**
     * constructor.
     *
     */
    public function __construct()
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix.'WP_PAYZOS_PAYMENT_WOOCOMMERCE';
        $this->db = $wpdb;
    }

    /**
     * Installa method to register my database
     *
     * @return boolean
     */
    public static function install()
    {
        global $wpdb;
        $table_name = $wpdb->prefix.'WP_PAYZOS_PAYMENT_WOOCOMMERCE';
        /**
         * database structure
         * id
         * order_id : store woocommerce order_id
         * amount : xtz amount * 1000000
         * destination_hash : admin tezos hash_id (stored in woocommerce payment configure).
         * origin_hash : after transaction done, we will store which hash id did this transaction.
         * transaction_hash :‌ group block hash (get from conseil)
         * start_time : unixTime.
         * finish_time : unixTime.
         * status : - done - fail - await.
         */
        $sql = " CREATE TABLE ".$table_name." (
            id                  BIGINT(20)   NOT NULL AUTO_INCREMENT,
            order_id            BIGINT(20)   NOT NULL,
            amount               BIGINT(30)  NOT NULL,
            destination_hash    VARCHAR(200) NOT NULL,
            origin_hash         VARCHAR(200)     NULL,
            transaction_hash          VARCHAR(200)     NULL,
            start_time          BIGINT(20)   NOT NULL,
            finish_time         BIGINT(20)       NULL,
            status              VARCHAR(20)  NOT NULL,
            PRIMARY KEY  (id)
        )".$wpdb->get_charset_collate().";";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        add_option('jal_db_version', '1.0.0');
        if ($wpdb->get_var("SHOW TABLES LIKE '".$table_name."'") != $table_name) {
            error_log("unable to make db");
            return false;
        }
        return true;
    }

    /**
     * store initial payment data
     *
     * @param int       $_order_id
     * @param int       $_amount
     * @param string    $_destination_hash
     * @param int       $_time
     * @return int      $payment_id
     */
    public function store_payment($_order_id, $_amount, $_destination_hash, $_time)
    {
        if (!is_int($_order_id) || !is_int($_amount) || !is_string($_destination_hash) || !is_int($_time)) {
            return false;
        }
        $query = $this->db->insert(
            $this->table_name,
            [
            "order_id"          => $_order_id,
            "amount"             => $_amount,
            "destination_hash"  => $_destination_hash,
            "start_time"        => $_time,
            "status"            => "await",
            ],
            [
            "%d",
            "%d",
            "%s",
            "%d",
            "%s",
            ]
        );
        if (!$query) {
            return false;
        }
        $payment_id = $this->db->insert_id;
        return (is_int($payment_id) ? $payment_id : false);
    }

    /**
     * update payment after finish
     *
     * @param int       $_payment_id
     * @param string    $_origin_hash
     * @param int       $_time
     * @return boolean
     */
    public function finish_payment($_payment_id, $_origin_hash, $_time, $_transaction_hash)
    {
        if (!is_int($_payment_id) || !is_string($_origin_hash) || !is_int($_time) || !is_string($_transaction_hash)) {
            // error_log($_payment_id . ":: " . . ":: " . . ":: " . );

            return false;
        }
        $query = $this->db->update(
            $this->table_name,
            [
            "origin_hash" => $_origin_hash,
            "finish_time" => $_time,
            "transaction_hash" => $_transaction_hash,
            "status"      => "done",
            ],
            [
                "id" => $_payment_id
            ],
            [
            "%s",
            "%d",
            "%s",
            "%s",
            ],
            [
                "%d"
            ]
        );
        if (!$query) {
            error_log("why?");
            return false;
        }
        if ($query == 0) {
            error_log("why not!");
            return false;
        }
        return true;
    }
    
    /**
     * update payment if fail
     *
     * @param int $_payment_id
     * @param int $_time
     * @return boolean
     */
    public function cancel_payment($_payment_id, $_time)
    {
        if (!is_int($_payment_id) || !is_int($_time)) {
            return false;
        }
        $query = $this->db->update(
            $this->table_name,
            [
            "finish_time" => $_time,
            "status"      => "fail",
            ],
            [
                "id" => $_payment_id
            ],
            [
            "%d",
            "%s",
            ],
            [
                "%d"
            ]
        );
        if (!$query) {
            return false;
        }
        if ($query == 0) {
            return false;
        }
        return true;
    }

    /**
     * i must to be sure there is not more than one running payment with a certain amount.
     *
     * @param int $_amount
     * @return boolean
     */
    public function can_pay_this_amount($_amount)
    {
        if (!is_numeric($_amount)) {
            return false;
        }
        $query = $this->db->query(
            $this->db->prepare(
                "SELECT id FROM ".$this->table_name." WHERE amount = %d AND status = 'await'",
                $_amount
            )
        );
        if (!$query || $query == 0) {
            return true;
        }
        return false;
    }

    /**
     * Undocumented function
     *
     * @param [type] $_payment_id
     * @return void
     */
    public function get_payment($_payment_id)
    {
        if (!is_numeric($_payment_id)) {
            return false;
        }
        $payment = $this->db->get_row(
            $this->db->prepare(
                "SELECT * FROM ".$this->table_name." WHERE id = %d",
                $_payment_id
            ),
            ARRAY_A,
            0
        );
        if (is_null($payment)) {
            return false;
        }
        $order = wc_get_order($payment["order_id"]);
        if (!is_numeric($order->get_total())) {
            return false;
        }
        $order_amount = $order->get_total();
        $order_currency = $order->get_currency();
        $payment["amount_orginal"] = $order_amount;
        $payment["currency_orginal"] = $order_currency;
        return $payment;
    }

    /**
     * Undocumented function
     *
     * @param string $_sort_by
     * @param boolean $_ASC
     * @param integer $_page
     * @return void
     */
    public function transaction_list($_sort_by = "id", $_ASC = false, $_page = 1)
    {
        $results = $this->db->get_results(
            $this->db->prepare(
                "SELECT * FROM " . $this->table_name . " ORDER BY %s %s LIMIT %d,%d",
                $_sort_by,
                (($_ASC) ? "ASC" : "DESC"),
                (0*$_page),
                (10*$_page)
            ),
            ARRAY_A
        );
        if (!$results || !is_array($results) || is_wp_error($results)) {
            return [];
        }
        return $results;
    }

    /**
     * get list of payments for run cron job
     *
     * @param integer $_time
     * @return array
     */
    public function get_long_await_payment($_time = 900)
    {
        if (!is_numeric($_time)) {
            return false;
        }
        $time = time() - $_time;
        $results = $this->db->get_results(
            $this->db->prepare(
                "SELECT id FROM " . $this->table_name . " WHERE start_time <= %d AND status = 'await'",
                $time
            ),
            ARRAY_A
        );
        error_log(json_encode($results));
        if (!$results || !is_array($results) || is_wp_error($results)) {
            return [];
        }
        return $results;
    }
}
