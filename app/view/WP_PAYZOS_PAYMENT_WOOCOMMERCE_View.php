<?php
class WP_PAYZOS_PAYMENT_WOOCOMMERCE_View
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'payment_page_scripts']);
    }


    public function payment_page_scripts()
    {
        wp_register_script('WP_PAYZOS_PAYMENT_WOOCOMMERCE_payment_script', WP_PAYZOS_PAYMENT_WOOCOMMERCE_ASSETS_URL .'payment-ui/build/bundle.js', array(), '1.0.0');
        wp_register_style('WP_PAYZOS_PAYMENT_WOOCOMMERCE_payment_style', WP_PAYZOS_PAYMENT_WOOCOMMERCE_ASSETS_URL .'payment-ui/build/bundle.css');
    }

    public function payment_page($_data)
    {
        $inline_script = "
            <script>
            const PAYZOS_API_URL = '".get_rest_url(null, 'wp_payzos_wc/v1/')."'
            const PAYZOS_TEZOS_ENDPOINT = '".WP_PAYZOS_PAYMENT_WOOCOMMERCE_CONSEIL_API_URL."';
            const PAYZOS_TEZOS_APIKEY = '".WP_PAYZOS_PAYMENT_WOOCOMMERCE_CONSEIL_API_KEY."';
            </script>
            <script src=\"https://cdn.jsdelivr.net/gh/cryptonomic/conseiljs/dist-web/conseiljs.min.js\"
        integrity=\"sha384-1Lpjkva0cskGzGmrvU+WIbzNk24LwcaEFgsukzbI5wTl7T6kom1UA4DKS/oxtaqr\"
        crossorigin=\"anonymous\"></script>
        ";
        wp_enqueue_style('WP_PAYZOS_PAYMENT_WOOCOMMERCE_payment_style');
        echo "<div style=\"max-width: 520px; margin:auto\" id=\"payzos_payment_page\"></div>";
        echo $inline_script;
        wp_enqueue_script('WP_PAYZOS_PAYMENT_WOOCOMMERCE_payment_script');
    }


    public function transaction_list_page($_data)
    {
        include WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR . 'view/template/transaction_list.php';
    }
}
