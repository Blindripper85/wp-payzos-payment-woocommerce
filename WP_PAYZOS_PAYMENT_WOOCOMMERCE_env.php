<?php
defined('ABSPATH') || exit('No Direct Access.');

/**
 * this file contein plugin enviroment constants
 */

/**
 * more information about Conseil :
 * - https://github.com/Cryptonomic/ConseilJS,
 * - https://cryptonomic.github.io/ConseilJS/
 * conseil api url and key get from : https://nautilus.cloud/home
 */
define("WP_PAYZOS_PAYMENT_WOOCOMMERCE_CONSEIL_API_URL", "https://conseil-prod.cryptonomic-infra.tech");
define("WP_PAYZOS_PAYMENT_WOOCOMMERCE_CONSEIL_API_KEY", "9258f47b-4b9a-493d-977e-44a9b8ee5876");
define("WP_PAYZOS_PAYMENT_WOOCOMMERCE_CONSEIL_PLATFORM", "tezos");
/**
 * we use Fixer Api for convert other type of currencies to USD
 */
define("WP_PAYZOS_PAYMENT_WOOCOMMERCE_FIXER_API_KEY", "0299c8395dbb841dbf7ca3f9b3cce60f");
/**
 * Some defines we need to encrypt a data
 */
define("WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_METHOD", "AES-256-CBC");
define("WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_KEY", "f5eec7ed408c42c1e8dd1e313906efe5");
define("WP_PAYZOS_PAYMENT_WOOCOMMERCE_HASH_IV", "aea45d7e27dab378b25b76e44a778199");
