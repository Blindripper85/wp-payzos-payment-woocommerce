<?php
/*
Plugin name: wp-payzos-payment-woocommerce
Plugin URI: https://gitlab.com/payzos/wp-payzos-payment-woocommerce
Description: unofficial payzos payment gateway for woocommerce
Version: 0.1.0
Author: Johan Peterson
Author URI:
Text Domain: -e('wp payzos woocommerce','wp-payzos-payment-woocommerce')
 */
defined('ABSPATH') || exit('No Direct Access.');
define('WP_PAYZOS_PAYMENT_WOOCOMMERCE_DIR', plugin_dir_path(__FILE__));
define('WP_PAYZOS_PAYMENT_WOOCOMMERCE_URL', plugin_dir_url(__FILE__));
define('WP_PAYZOS_PAYMENT_WOOCOMMERCE_ASSETS_URL', trailingslashit(WP_PAYZOS_PAYMENT_WOOCOMMERCE_URL.'assets'));
define('WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR', trailingslashit(WP_PAYZOS_PAYMENT_WOOCOMMERCE_DIR.'app'));
define('WP_PAYZOS_PAYMENT_WOOCOMMERCE_VERSION', "0.1.0");
include WP_PAYZOS_PAYMENT_WOOCOMMERCE_DIR . "WP_PAYZOS_PAYMENT_WOOCOMMERCE_env.php";
require WP_PAYZOS_PAYMENT_WOOCOMMERCE_APP_DIR . 'controller/WP_PAYZOS_PAYMENT_WOOCOMMERCE_Controller.php';
add_action('init', 'WP_PAYZOS_PAYMENT_WOOCOMMERCE_load');
function WP_PAYZOS_PAYMENT_WOOCOMMERCE_load()
{
    $app = new WP_PAYZOS_PAYMENT_WOOCOMMERCE_Controller();
}

register_activation_hook(__FILE__, 'WP_PAYZOS_PAYMENT_WOOCOMMERCE_Installation');

function WP_PAYZOS_PAYMENT_WOOCOMMERCE_Installation()
{
    WP_PAYZOS_PAYMENT_WOOCOMMERCE_Controller::install();
}
