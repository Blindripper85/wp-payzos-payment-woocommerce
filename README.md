
[![Payzos logo](/.gitassets/img/logo.png "Payzos logo")](https://payzos.io)


# Wordpress Payzos payment for Woocommerce
[Payzos](https://payzos.io) is a set of plugins for different e-commerce platforms that lets you set up [Tezos](https://tezos.com/) as a payment method for your online store.

# Version 1.0.0 Beta


# Installation

you must to put our plugin in `WORDPRESS/wp-content/plugins/`

then activeate it from wordpress dashboard

you can download last release or clone from git. 

## Pre release (git)
we have another git repository as a submodule in `assets/payment-ui`

you must to clone repo with `--recursive` flag or after clone you must to run this command :
```
// First Time
git submodule update --init --recursive
// For update and get last commit of payment-ui repository
git pull --recurse-submodules
```

- for more information : [+](https://stackoverflow.com/questions/1030169/easy-way-to-pull-latest-of-all-git-submodules)
- [payment-ui](https://gitlab.com/payzos/payment-ui) repository

## last release
download the last release from realease page on gitlab [last release](https://gitlab.com/payzos/wp-payzos-payment-woocommerce/-/releases)

## from wordpress official repository
Coming soon.

# plugin configuration
first you must to install woocommerce ( without woocommerce plugin has not any affect)\
then go to `woocommerce > settings > payments > payzos > setting`\
add your **wallet hash** and save (also enable payment).\
make a wordpress page with this address : `YOURSITE_ADDRESS/payzos` and put this shortcode on it `[wp-payzos-payment-woocommerce]` (it's important without this page payment will not work.)\
alright your payment is ready.

# Changelog
### V0.1.0
- init payzos payment plugin
### V1.0.0 Beta:
- a good ui for payment page (with react, watch [payment-ui](https://gitlab.com/payzos/payment-ui))
- check a wallet address to verify a payment with a time and amount.
- wordpress rest api for manage payment.
- check is valid wallet hash in payment setting (default woocommerce payments page)
